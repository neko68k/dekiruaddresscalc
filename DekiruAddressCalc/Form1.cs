﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DekiruAddressCalc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void butGo_Click(object sender, EventArgs e)
        {
            uint num =  UInt32.Parse(tbFrom.Text, System.Globalization.NumberStyles.HexNumber);
            num -= 0x8003B800;
            tbFromResult.Text = num.ToString("X5");

            num = UInt32.Parse(tbTo.Text, System.Globalization.NumberStyles.HexNumber);
            num += 0x8003B800;
            tbToResult.Text = num.ToString("X8");
        }
    }
}
